<?php

    get_header();
?>

<div class="contact">
    <div class="container">
        <h1>Contact Us</h1>
        <p>Kindly fill out the form complete and please make sure that details are all valid.</p>

        <?php echo do_shortcode('[contact-form-7 id="148" title="ODL Speaking Engagement Form"]'); ?>
    </div>
</div>

<?php
    get_footer();
?>