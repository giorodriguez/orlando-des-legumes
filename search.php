<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package orlandodeslegumes
 */

get_header();
?>
	<div class="container">
		<div class="search-results">	
			<h1>Search Results</h1>
			<p class="search-results-number"> Your Search Results for: <?php echo get_search_query(); ?></p>

			<section id="primary" class="content-area">
				<main id="main" class="site-main">
				<?php
					function displaySearchResults($category) {
						$query_args = array(
							'category_name' => ucfirst($category),
							's' => get_search_query()
						);

						$query = new WP_Query($query_args);
						if($query->have_posts()):  
							while($query->have_posts()):  
								$query->the_post();
								get_template_part('templates-search-results/results', $category);
							endwhile;
						else:  
							get_template_part('template-parts/content', 'none');
						endif;
					}

					echo "<h3>Blogs</h3>";
					displaySearchResults('blogs');
					echo "<h3>Testimonials</h3>";
					displaySearchResults('testimonials');
				?>
				</main>
			</section>
		</div>
	</div>
<?php
// get_sidebar();
get_footer();
