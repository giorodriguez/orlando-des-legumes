<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orlandodeslegumes
 */

?>

	</div>

	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="footer-site-info">
					<div class="footer-site-logo">
						<?php echo get_custom_logo(); ?>
						<p><?php echo get_bloginfo();?></p>
					</div>

					<div class="copyright">
						<p>&copy; 2018 KG Web Services. All rights reserved.</p>
					</div> 
				</div>

				<div class="col-md-4" id="footer-main">
					<nav class="footer-navigation">
						<?php
							wp_nav_menu( array(
								'menu' => 'Main Menu',
								'menu_class' => 'secondary-menu',
								'menu_id' => 'footer-menu',
							) );
						?>
					</nav>

					<div class="contact-details">
						<p>Manila, Philippines</p>
						<p>+639164345955</p>
						<p>+6326631612</p>
					</div> 
				</div>

				<div class="col-md-4" id="footericons">
					<div class="social-icons">
						<a href="https://www.facebook.com/Orlandos-Des-Legumes-559797311118041/" target="_blank"><img src="<?php echo get_template_directory_uri().'/images/facebook-footer.png'?>" class="social-icon"></a>
						<a href="mailto:orlandosdeslegumes@gmail.com"><img src="<?php echo get_template_directory_uri().'/images/mail-footer.png'?>" class="social-icon"></a>
						<a href="#" target="_blank"><img src="<?php echo get_template_directory_uri().'/images/instagram-footer.png'?>" class="social-icon"></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
