<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orlandodeslegumes
 */

get_header();
?>
	<!-- <div class="loading-screen">
		<div class="loader">
			<img src="<?php echo get_template_directory_uri().'/images/ODL-Logo4.1.png'?>" id="loading-logo">
			<img src="<?php echo get_template_directory_uri().'/images/loader.gif'?>" id="loader">
			<p>Loading please wait...</p>
		</div>
	</div> -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="slider">
				<?php if( function_exists('cyclone_slider') ) cyclone_slider('odl-slider'); ?>	
			</div>

			<div class="testimonials">
				<div class="overlay"></div>
				<div class="container">
					<div class="testimonial-slider">	
						<?php
							$args = array(
								'category_name' => 'Testimonials'
							);

							$testimonials = new WP_Query($args);

							if($testimonials->have_posts()): 
								while($testimonials->have_posts()):
									$testimonials->the_post();
									get_template_part('templates-homepage/loop','testimonials');
								endwhile;
							endif;
						?>
					</div>
				</div>
				<div class="testimonial-slider-controls">
					<img src="<?php echo get_template_directory_uri().'/images/slider-prev.png';?>" class="slider-control" id="prev">
					<img src="<?php echo get_template_directory_uri().'/images/slider-next.png';?>" class="slider-control" id="next">
				</div>
			</div>

			<div class="gallery">
				<div class="container">
					<h2>Our Gallery</h2>
					<div class="gallery-border"></div>
					<div class="gallery-slider">
						<?php 
							$gallery_query = array(
								'category_name' => 'Gallery',
								'order' => 'ASC',
								'orderby' => 'date'
							);
							$gallery = new WP_Query($gallery_query);
							if($gallery->have_posts()): 
								while($gallery->have_posts()): 
									$gallery->the_post();
									get_template_part('templates-homepage/loop','gallery');
								endwhile;
							endif;
						?>
					</div>
				</div>
			</div>

		</main>
	</div>

<?php
// get_sidebar();
get_footer();
