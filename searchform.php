<div class="search">
    <form id="search-form" class="search-form" role="search" action="<?php echo home_url();?>">
        <input type="text" name="s" placeholder="Search" autocomplete="off">
    </form>
</div>