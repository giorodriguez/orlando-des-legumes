<?php
/* Template part for displaying search results for testimonials */
?>

<div class="result">
	<p class="title"><?php the_title(); ?></p>
	<p class="excerpt"><?php the_excerpt(); ?></p>
</div>