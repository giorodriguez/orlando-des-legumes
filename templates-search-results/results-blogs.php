<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orlandodeslegumes
 */

?>

<div class="result">
	<p class="title"><?php the_title(); ?></p>
	<p class="excerpt"><?php the_excerpt(); ?></p>
	<a href="<?php echo get_permalink();?>" class="link">Read Article "<?php the_title(); ?>"</a>
</div>
