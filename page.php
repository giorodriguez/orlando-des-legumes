<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orlandodeslegumes
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container default">
				<h3>Sorry!</h3>
				<p>Page under construction</p>
				<img src="<?php echo get_template_directory_uri().'/images/default-page-img.png'?>">
				<p>Content will be available soon!</p>
			</div>
		</main>
	</div>

<?php
get_footer();
