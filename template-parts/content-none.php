<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orlandodeslegumes
 */

?>

<section class="no-results not-found">
	<p>No results found for: <?php echo get_search_query();?></p>
</section><!-- .no-results -->
