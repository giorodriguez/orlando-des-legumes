<?php
    /* Template part for blogs */
?>

<div class="blog">
    <div class="blog-thumbnail">
        <?php echo the_post_thumbnail();?>
    </div>
    <div class="blog-content">
        <h1><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h1>
        <p><?php the_excerpt();?></p>
        <span>By: <?php the_author();?></span>
        <span><?php echo get_the_date();?></span>
    </div>
</div>