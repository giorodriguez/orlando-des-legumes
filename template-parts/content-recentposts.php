<?php
/*Template part for recent posts*/
?>
<div class="recentpost">
    <a href="<?php echo get_permalink(); ?>" class="title"><?php the_title(); ?></a>
    <p class="dateposted"><?php echo get_the_date(); ?></p>
</div>
