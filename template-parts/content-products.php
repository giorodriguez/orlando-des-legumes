<?php
/* Template file for products */
?>

<div class="product">
    <a href="<?php echo get_the_post_thumbnail_url(); ?>" data-lightbox="vegetable-image" data-title="<?php the_content();?>"><?php the_post_thumbnail(array('class_name' => 'product-image')); ?></a>
    <p class="product-name"><?php the_title(); ?></p>
</div>