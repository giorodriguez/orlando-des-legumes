<?php
/* Template part for gallery page*/

?>
<div class="gallery-image">
    <?php the_post_thumbnail();  ?>
    <a href="<?php echo get_the_post_thumbnail_url(); ?>" data-lightbox="gallery-image" data-title="<?php the_title();?>">
        <div class="gallery-image-info">
            <h3><?php the_title(); ?></h3>
        </div>
    </a>
</div>