<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orlandodeslegumes
 */

?>
<div class="blog-post">
	<h1><?php the_title(); ?></h1>
	<?php the_post_thumbnail(); ?>
	<p class="blog-content"><?php the_content(); ?></p>
	<span>Posted By: <?php the_author(); ?> </span>
	<span><?php echo get_the_date(); ?></span>
</div>
