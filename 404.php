<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package orlandodeslegumes
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container not-found-page">
				<div class="row">
					<div class="col-sm-6">
						<img class="pumpkin-img" src="<?php echo get_template_directory_uri().'/images/404-pumpkin.png'?>">
					</div>
					<div class="col-sm-6">
						<p class="page-not-found">Page Not Found!</p>
						<img class="status-img" src="<?php echo get_template_directory_uri().'/images/404.png'?>">
						<img class="pepper-img" src="<?php echo get_template_directory_uri().'/images/404-pepper.png'?>">
						<p class="page-not-found-message">Oops. Sorry! The page you are trying to reach doesn’t exist!</p>
						<a href="<?php echo home_url();?>" class="redirect-btn">Home</a>
					</div>
				</div>
			</div>
		</main>
	</div>

<?php
get_footer();
