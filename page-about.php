<?php
    get_header();
?>
<div class="about-page">
    <div class="container">
        <h2>Welcome to our Business</h2>
        <div class="about-border"></div>

        <div class="about" id="history">
            <h3>Our brief history</h3>
            <p>Orlando’s Des Legumes was named after our father. “Des Legumes” was a french word which means “Vegetables” so that we decided to combined it next to our father’s name.</p>
        </div>

        <div class="about" id="about">
            <h3>Who we are & what we do</h3>
            <p>We are from Apolonio’s family and we are a vegetable supplier from the roots of Northern Samar and was given an opportunity to be part of Province of Benguet. We are farmers traders and became a restaurant supplier  of vegetables, eggs and rice.</p>
        </div>

        <div class="about-profile-header">
            <p>Know More About Us</p>
            <div class="about-header-border"></div>
        </div>

        <div class="profile">
            <div class="profile-image">
                <div class="about-image"></div>
            </div>
            <div class="profile-info">
                <div class="about-profile">
                    <p class="name">Full Name</p>
                    <p class="position">Owner</p>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>
            </div>
        </div>        
    </div>
</div>
<?php
    get_footer();
?>