<?php
get_header();

function getProducts($category) {
    $args = array(
        'category_name' => ucfirst($category)
    );

    $products = new WP_Query($args);
    if($products->have_posts()): 
        while($products->have_posts()):  
            $products->the_post();
            get_template_part('template-parts/content', 'products');
        endwhile;
    endif;
}
?>
<div class="products-page">
    <div class="container">
        <div class="vegetable-container">
            <div class="vegetables-header">
                <p>Vegetables</p>
            </div>

            <div class="products">
                <?php getProducts('vegetables'); ?>
            </div>
        </div>
        <div class="vegetable-container">
            <div class="vegetables-header">
                <p>Vegetables</p>
            </div>

            <div class="products">
                <?php getProducts('vegetables'); ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>