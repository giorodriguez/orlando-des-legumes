<?php
/* Template part for testimonials in homepage*/
?>
<div class="testimonial">
    <h1><?php echo the_title();?></h1>
    <div class="quote"><?php echo the_content();?></div>
    <p class="author"></p>
</div>