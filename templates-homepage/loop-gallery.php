<?php
/* Template part for testimonials in homepage*/
?>
<div class="thumbnail">
    <a href="<?php echo get_the_post_thumbnail_url();?>" data-lightbox="homepage-gallery"><?php the_post_thumbnail(); ?></a>
</div>