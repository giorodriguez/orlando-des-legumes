<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orlandodeslegumes
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header class="site-header">
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<div class="site-icon">
							<?php echo get_custom_logo(); ?>
						</div>

						<div class="text-icon">
							<p><?php echo get_bloginfo();?></p>
						</div>
					</div>

					<div class="col-sm-4 header-search">						
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>

		<!-- DEFAULT MENU -->
		<div class="main-menu">
			<div class="container">
				<div class="row">	
					<div class="col-sm-8 main-menu-col">
						<nav class="main-navigation">
							<?php
								wp_nav_menu( array(
									'menu' => 'Main Menu',
									'menu_id' => 'primary-menu'
								) );
							?>
						</nav>
					</div>
					
					<div class="col-sm-4">
				
					</div>
				</div>
			</div>
			<div class="menu-bottom-border"></div>
		</div>
			
		<!-- MOBILE MENU -->
		<div class="mobile-menu">
			<img src="<?php echo get_template_directory_uri().'/images/odl-menu-toggle-bar.png';?>" id="menu-toggle">
			
			<div class="mobile-search">
				<?php get_search_form(); ?>
			</div>
		</div>
		
		<nav class="mobile-navigation">
		<button class="close-nav">&times;</button>
		<?php
			wp_nav_menu( array(
				'menu' => 'Main Menu',
				'menu_id' => 'mobile-menu'
			) );
		?>
		</nav>
	
	</header>

	<div id="content" class="site-content">
