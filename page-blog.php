<?php 
    get_header();
?>
    <div class="blogs">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <?php
                        $args = array(
                            'category_name' => 'Blogs',
                            'meta_query' => array(array('key' => '_thumbnail_id'))
                        );
                        $blogs = new WP_Query($args);
                        if($blogs->have_posts()):  
                            while($blogs->have_posts()):
                                $blogs->the_post();  
                                get_template_part('template-parts/content','blogs');
                            endwhile;
                        endif;
                    ?>
                </div>

                <div class="col-md-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
<?php 
    get_footer();
?>