<?php
get_header();
?>
<div class="gallery-page">
    <div class="container">
        <h3>Gallery</h3>
        <div class="gallery-page-border"></div>
        <div class="masonry">
            <?php
                $args = array(
                    'category_name' => 'Gallery'  
                );

                $gallery = new WP_Query($args);

                if($gallery->have_posts()):  
                    while($gallery->have_posts()):  
                        $gallery->the_post();
                        get_template_part('template-parts/content', 'gallery');
                    endwhile;
                endif;
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>