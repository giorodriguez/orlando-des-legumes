<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orlandodeslegumes
 */
?>
<aside class="blog-page-sidebar">
	<div class="about-sidebar-section">
		<h3>About Orlando's Des Legumes</h3>
		<p>We are are a vegetables supplier from the roots of Northern Samar and was given an opportunity to be part of Province of Benguet we are farmers traders and became a restaurant supplier of vegetables, eggs and rice.</p>
	</div>
	<div class="sidebar-border"></div>
	<div class="posts-sidebar-section">
		<h3>Recent Posts</h3>
		<?php
			$args = array(
				'category_name' => 'Blogs',
				'order' => 'DESC',
				'orderby' => 'date',
				'posts_per_page' => '4'
			);

			$recentPosts = new WP_Query($args);

			if($recentPosts->have_posts()):  
				while($recentPosts->have_posts()):  
					$recentPosts->the_post();
					get_template_part('template-parts/content', 'recentposts');
				endwhile;
			endif;
		?>
	</div>
</aside>
