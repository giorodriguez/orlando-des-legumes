$(document).ready(function(){
    /* Loading Screen */


    /* Slick Slider */
    $(".testimonial-slider").slick({
        slidesToShow:1,
        slidesToScroll:1,
        infinite:true,
        dots:false,
        autoplay:true,
        autoplaySpeed:5000,
        prevArrow:'#prev',
        nextArrow:'#next'
    });

    $(".gallery-slider").slick({
        slidesToShow:3,
        slidesToScroll:1,
        dots:false,
        prevArrow:'',
        nextArrow:'',
        infinite:true,
        responsive: [
            {
                breakpoint:768,
                settings: {
                    slidesToShow:2,
                    slidesToScroll:2
                }
            },
            {
                breakpoint:481,
                settings: {
                    slidesToShow:1,
                    slidesToScroll:1
                }
            }
        ]
    });

    $("#menu-toggle").click(function(){
        $(".mobile-navigation").css({"width":"40%"});
    });

    $(".close-nav").click(function(){
        $(".mobile-navigation").css({"width":"0"});
    });
});
    